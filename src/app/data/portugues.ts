import { IQuestion } from "../interfaces/IQuestion"

export const questions = [
  {
    id: 1,
    description: "Classifique a palavra",
    word: "Gato",
    classifications: ["Artigo", "Substantivo", "Adjetivo", "Verbo"],
    correct: "Substantivo"
  },
  {
    id: 2,
    description: "Classifique a palavra",
    word: "Fazer",
    classifications: ["Artigo", "Substantivo", "Adjetivo", "Verbo"],
    correct: "Verbo"
  },
]