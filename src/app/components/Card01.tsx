"use client"

import { useState } from "react";
import Image from "next/image";
import Feedback from "./Feedback";

export default function Card01(questions: any) {
  const [current, setCurrent] = useState(0);
  const [selectedAnswer, setSelectedAnswer] = useState(-1);
  const [isAnswered, setIsAnswered] = useState(0);
  const [isCorrect, setIsCorrect] = useState(false);
  const [isComplete, setIsComplete] = useState(false);
  const [total, setTotal] = useState(0);

  const totalQuestions = questions.data.length;

  const handleClick = (value: string) => {
    if (value === questions.data[current].correct) {
      if(isAnswered == 0) {
        setTotal(total + 1);
      }
      setIsCorrect(true);
    } else {
      setIsCorrect(false);
    }

    setIsAnswered(isAnswered + 1);
  }

  const handleNext = () => {
    setIsAnswered(0);
    setSelectedAnswer(-1);

    if ((current + 1) != totalQuestions) {
      setCurrent(current + 1);
    } else {
      setIsComplete(true);
    }
  }

  const restart = () => {
    setIsComplete(false);
    setTotal(0);
    setCurrent(0);
  }

  return (
    <>
      <div className="my-4 py-4">
        { isComplete && (<Feedback value={total} restart = {restart} />) }

        {!isComplete && (
          <div>
            <div className="my-8 py-4 px-2 bg-gray-100 rounded-lg text-center text-2xl text-violet-700 border">
              {questions.data[current].description}
            </div>

            <div className="mb-6 flex justify-center">
              <div className="bg-violet-400 py-6 px-16 text-5xl">
                {questions.data[current].word}
              </div>
            </div>

            { !!isAnswered && (
              <div className="flex justify-center p-2">
                <div className="p-6 rounded-full">
                  <Image
                    src={(isCorrect) ? "/gifs/correct_answer.gif" : "/gifs/incorrect_answer.gif"}
                    width="0"
                    height="0"
                    alt="success"
                    className="w-16 h-auto"
                  />
                </div>
              </div>
            )}

            <div className="flex flex-col items-center justify-center h-auto">
              {
                questions.data[current].classifications.map((classification: string, index: number) => (
                  <div key={index}
                    className={
                      `${(selectedAnswer == index && isCorrect) ?
                        'bg-green-200 hover:bg-green-200' :
                        (selectedAnswer == index && !isCorrect) ? 'bg-red-200' : 'bg-gray-200'}
                        p-4 m-2 w-56 text-center rounded-md cursor-pointer`
                    }
                    onClick={() => [handleClick(classification), setSelectedAnswer(index)]}
                  >
                    {classification}
                  </div>
                ))
              }
            </div>

            

            { !!(isAnswered && isCorrect) && (
              <div className="flex justify-center">
                <button
                  className="bg-blue-200 px-4 py-2 text-sm rounded-lg"
                  onClick={handleNext}
                >
                  Próxima
                </button>
              </div>
            )}
          </div>
        )}
      </div>
    </>
  )
}
