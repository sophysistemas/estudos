
export default function Feedback({ value, restart }: any) {

  return (
    <div className="my-8 py-4 px-2 flex flex-col items-center bg-gray-100 rounded-lg text-2xl text-violet-700 border">
      <div className="my-4">
        <span>Exercício finalizado!!!</span>
      </div>

      <div className="my-4">
        <span className="text-xl">Sua pontuação foi</span>
        <div
          className={`${value <= 5 ? 'bg-red-200 text-red-500' : value >= 8 ? 'bg-blue-200 text-blue-500' : 'bg-yellow-100 text-yellow-600'} text-5xl font-bold px-8 py-14 my-8 flex justify-center rounded-full border`}>
          {value}
        </div>
      </div>

      <div className="flex justify-center"> 
        <button
          className="bg-blue-200 px-4 py-2 text-sm rounded-lg"
          onClick={restart}
        >
          Tentar novamente
        </button>
      </div>
    </div>
  )
}
