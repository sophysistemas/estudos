export interface IQuestion {
  id: number,
  description: string;
  word?: string,
  classifications?: string[],
  correct?: string
}