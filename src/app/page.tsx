import Header from "./components/Header";

export default function Home() {
  return (
    <>
      <Header />
      <div className="flex justify-between items-center p-3 max-w-6xl mx-auto">
        
        <h1>Meu App de estudos</h1>

      </div>
    </>
  );
}
