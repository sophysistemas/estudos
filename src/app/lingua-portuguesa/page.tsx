import Header from "../components/Header";
import { questions } from "../data/portugues";
import Card01 from "../components/Card01";
import { IQuestion } from "../interfaces/IQuestion";

export default function Home() {
  return (
    <>
      { /*<Header /> */}
      <div className="my-8 p-3 max-w-6xl mx-auto">
        <h1 className="text-normal text-3xl text-center font-bold text-blue-700">Exercícios de Língua Portuguesa</h1>
          <Card01 data={questions} />
      </div>
    </>
  );
}
